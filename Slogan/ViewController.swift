//
//  ViewController.swift
//  Slogan
//
//  Created by Матвей Кравцов on 17.07.15.
//  Copyright (c) 2015 Матвей Кравцов. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet var field : UITextField!
    @IBOutlet var result : UILabel!
    
    
    //Пока бесполезна =)
    @IBAction func viewTapped(sender : AnyObject) {
        field.resignFirstResponder()
    }
    
    @IBAction func CopySlogan(sender: AnyObject) {
        UIPasteboard.generalPasteboard().string = result.text
    }
    
    @IBAction func GetSlogan(sender: AnyObject) {
        if (field.text == "") {
            var alert = UIAlertController(title: "Ошибочка", message: "Пустое поле ввода", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { action in
                self.dismissViewControllerAnimated(true, completion: nil)
            }))
            self.presentViewController(alert, animated: true, completion: nil)
            return
        }
        let url: NSURL = NSURL(string: "http://erfa.ru/index.php")!
        let request:NSMutableURLRequest = NSMutableURLRequest(URL:url)
        request.HTTPMethod = "POST"
        let bodyData = "slovoInput="+field.text!
        request.HTTPBody = bodyData.dataUsingEncoding(NSUTF8StringEncoding);

        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue()){
            (response: NSURLResponse?, data: NSData?, error: NSError?) in
            
                let output = (NSString(data: data!, encoding: NSUTF8StringEncoding) as? String)!
        
            
                let i1: String.Index! = output.rangeOfString("<h3 class=\"generate\">")?.endIndex
                var i2: String.Index! = output.rangeOfString("</h3>")?.startIndex
                i2 = advance(i2!, -1)
                let sub = output.substringWithRange(i1...i2)
                self.result.text = sub
        }
        
        view.endEditing(true);

    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

